#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <bits/stdc++.h>
#include <chrono>
#include <random>

#define DIR "/home/maria/PDEEC/Heuristics/SCP/SCP-Instances/"

// Checks if all the elements are covered
int check_elements(std::vector<int> &covered_elements)
{
    int sum = 0;

    for(int i = 0; i < covered_elements.size(); i++)
    {
        sum += covered_elements[i];
    }

    return sum;
}

// Computes total cost of solution
int evaluate_solution(std::vector<int> &selected_subsets, std::vector<int> &weights)
{
    int total_cost = 0;

    for(int i = 0; i < selected_subsets.size(); i++)
    {
        total_cost += weights[selected_subsets[i]];
    }

    return total_cost;
}

// Dispatch rule of constructive heuristic 1
int ch1(int number_subsets, std::vector<std::vector<int>> &subsets_backup, std::vector<int> &weights){

    int selected_subset;
    std::vector<float> coefficients (number_subsets);

    //compute coefficient for each subset (number of elements of subset / weight of subset)
    for(int i = 0; i < number_subsets; i++)
    {
        coefficients[i] = (float)subsets_backup[i].size()/weights[i];
    }

    //check maximum coefficient
    auto it = std::max_element(std::begin(coefficients), std::end(coefficients));
    selected_subset = std::distance(std::begin(coefficients),it);

    return selected_subset;
}

// Dispatch rule of constructive heuristic 2
int ch3(int number_elements, std::vector<std::vector<int>> &subsets_by_element, std::vector<int> &covered_elements){

    //get rarest element which is not yet covered
    int rare_element = 0;
    int n_subsets = 9999999;
    for(int i = 0; i < number_elements; i++)
    {
        if(subsets_by_element[i].size() < n_subsets && covered_elements[i] == 0)
        {
            rare_element = i;
            n_subsets = subsets_by_element[i].size();
        }
    }

    // select subset which covers said element
    int subset = subsets_by_element[rare_element][0];

    return subset;
}

// Dispatch rule of constructive heuristic 3
int ch2(int number_elements, std::vector<std::vector<int>> &subsets_by_element, std::vector<int> &covered_elements){

    std::vector<int> uncovered;

    for(int i = 0; i < number_elements; i++)
    {
        if(covered_elements[i] == 0)
        {
            uncovered.push_back(i);
        }
    }

    //get random element which is not yet covered
    int random_element = uncovered[rand() % uncovered.size()];

    // select subset which covers said element
    int subset = subsets_by_element[random_element][0];

    return subset;
}

// Redundancy Elimination Procedure
void eliminate_redundancy(int number_elements, int number_subsets, std::vector<int> &selected_subsets, std::vector<int> &weights, std::vector<std::vector<int>> &subsets, std::vector<std::vector<int>> &selected_subsets_by_element){ 

    while(1)
    {        
        //copy selected subsets
        std::vector<int> redundant_sets = selected_subsets;
        
        //all subsets which are the sole coverers of any element cannot be eliminated
        for(int i = 0; i < number_elements; i++)
        {
            if(selected_subsets_by_element[i].size() == 1)
            {
                auto it = find(std::begin(redundant_sets),std::end(redundant_sets),selected_subsets_by_element[i][0]);
                if(it != std::end(redundant_sets))
                {
                    redundant_sets.erase(it);
                }
            }
        }

        if(redundant_sets.size() == 0)
            return;

        int selected_subset;
        std::vector<float> coefficients (redundant_sets.size());
        float min_coeff = 999999;
        int min_coeff_sub;

        //compute coefficient for each subset (number of elements of subset / weight of subset)
        for(int i = 0; i < redundant_sets.size(); i++)
        {
            coefficients[i] = (float)subsets[redundant_sets[i]].size()/weights[i];

            if(coefficients[i] < min_coeff)
            {
                min_coeff = coefficients[i];
                min_coeff_sub = redundant_sets[i];
            }
        }

        //check minimum coefficient
        auto it = find(std::begin(selected_subsets),std::end(selected_subsets), min_coeff_sub);
        int index = *it;

        // ELIMINATE SUBSET
        selected_subsets.erase(it);

        // Update selected subsets
        for(int i = 0; i < subsets[index].size(); i++)
        {
            int element = subsets[index][i];
            auto to_remove = find(std::begin(selected_subsets_by_element[element]),std::end(selected_subsets_by_element[element]),index);
            if(to_remove != std::end(selected_subsets_by_element[element]))
                selected_subsets_by_element[element].erase(to_remove);
        }
    }
}

// Local Search (First and Best Improvement)
int local_search(bool first, std::vector<int> &selected_subsets, std::vector<int> &unselected_subsets, int number_elements, int number_subsets, std::vector<int> &weights, std::vector<std::vector<int>> &subsets, std::vector<std::vector<int>> &selected_subsets_by_element){

    int init_total_cost = evaluate_solution(selected_subsets, weights), new_total = init_total_cost, new_best = init_total_cost;

    for(int i = 0; i < unselected_subsets.size(); i++)
    {        
        //add subset in line to solution
        std::vector<int> selected_subsets_fn = selected_subsets;
        selected_subsets_fn.push_back(unselected_subsets[i]);

        std::vector<std::vector<int>> selected_subsets_by_element_backup = selected_subsets_by_element;

        for(int j = 0; j < subsets[unselected_subsets[i]].size(); j++)
        {
            selected_subsets_by_element_backup[subsets[unselected_subsets[i]][j]].push_back(unselected_subsets[i]);
        }

        //eliminate redundancy
        eliminate_redundancy(number_elements, number_subsets, selected_subsets_fn, weights, subsets, selected_subsets_by_element_backup);    
        
        //check if new solution is better than initial
        new_total = evaluate_solution(selected_subsets_fn,weights);
        if(new_total < new_best)
        {
            selected_subsets = selected_subsets_fn;
            selected_subsets_by_element = selected_subsets_by_element_backup;
            new_best = new_total; 
            
            // exit if strategy is First Improvement
            if(first)
                return new_total;
        }
    }

    if(new_best >= init_total_cost)
    {
        return 0;
    }
}

int main(){
    
    std::cout << "Hello" << std::endl;

    for(int k = 0; k < 5; k++)
    {

        std::cout << "Running " << k << std::endl;

        //open intances file
        std::ifstream genfile(DIR + std::string("instances.txt"));
        std::string instance;
        int benchmark;

        //open results file
        std::ofstream resultsfile, timefile;
        resultsfile.open(DIR + std::string("improvement/new/results" + std::to_string(k+1) + ".csv"));
        timefile.open(DIR + std::string("improvement/new/time" + std::to_string(k+1) + ".csv"));
        resultsfile << "Inst \t Benchmark \t CH1 \t Deviation \t CH1-FI \t Deviation \t CH1-BI \t Deviation \t CH2 \t Deviation \t CH2-FI \t Deviation \t CH2-BI \t Deviation \t CH3 \t Deviation \t CH3-FI \t Deviation \t CH3-BI \t Deviation \t CH1-RE \t Deviation \t CH1-RE-FI \t Deviation \t CH1-RE-BI \t Deviation \t CH2-RE \t Deviation \t CH2-RE-FI \t Deviation \t CH2-RE-BI \t Deviation \t CH3-RE \t Deviation \t CH3-RE-FI \t Deviation \t CH3-RE-BI \t Deviation" << std::endl;
        timefile << "Inst \t CH1 \t CH1-FI \t CH1-BI \t CH2 \t CH2-FI \t CH2-BI \t CH3 \t CH3-FI \t CH3-BI \t CH1-RE \t CH1-RE-FI \t CH1-RE-BI \t CH2-RE \t CH2-RE-FI \t CH2-RE-BI \t CH3-RE \t CH3-RE-FI \t CH3-RE-BI" << std::endl;

        // 6 different algorithms (CH1, CH2 and CH3, with and without RE)
        int n_heuristics = 6;

        srand(time(NULL));

        // for each instance
        while(genfile >> instance)
        {
            //open instance file
            std::ifstream instfile(DIR + std::string("scp") + instance + std::string(".txt"));

            std::cout << "Instance " << instance << std::endl;

            genfile >> benchmark;
            resultsfile << instance << "\t" << benchmark;
            timefile << instance;

            //save number of subsets and number of elements
            int number_elements, number_subsets;
            instfile >> number_elements >> number_subsets;

            //save weight of each subset
            std::vector<int> weights;
            for(int i = 0; i < number_subsets; i++)
            {
                int weight;
                instfile >> weight;
                weights.push_back(weight);
            }

            std::vector<std::vector<int>> subsets;
            subsets.resize(number_subsets);

            std::vector<std::vector<int>> subsets_by_element;
            subsets_by_element.resize(number_elements);

            //save elements of each subset
            for(int i = 0; i < number_elements; i++)
            {
                int size;
                instfile >> size;
                for(int j = 0; j < size; j++)
                {
                    int subset;
                    instfile >> subset;
                    subsets[subset-1].push_back(i);
                    subsets_by_element[i].push_back(subset-1);
                }
            }

            // EXECUTE EACH CONSTRUCTIVE HEURISTIC
            for(int j = 0; j < n_heuristics; j++)
            {
                std::cout << "HEURISTIC " << j << std::endl;

                //start counter
                std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
            
                //create auxiliary structures
                std::vector<std::vector<int>> subsets_backup = subsets;

                //create output structures
                std::vector<int> covered_elements (number_elements);
                std::vector<int> selected_subsets;
                std::vector<std::vector<int>> selected_subsets_by_element;
                selected_subsets_by_element.resize(number_elements);

                while(check_elements(covered_elements) < number_elements)
                {
                    //Select subset to add to the solution (Dispatch Rule)
                    int index;
                    if(j == 0 || j == 3)  index = ch1(number_subsets, subsets_backup, weights);
                    else if(j == 1 || j == 4) index = ch2(number_elements, subsets_by_element, covered_elements);
                    else if(j == 2 || j == 5) index = ch3(number_elements, subsets_by_element, covered_elements);

                    //add subset to selected subsets
                    selected_subsets.push_back(index);

                    //add subset to selected subsets by element
                    for(int i = 0; i < subsets[index].size(); i++)
                    {
                        selected_subsets_by_element[subsets[index][i]].push_back(index);
                    }

                    int size = subsets_backup[index].size();
                    std::vector<int> newly_covered;

                    //update covered elements
                    for(int i = 0; i < size; i++)
                    {
                        //update covered elements
                        covered_elements[subsets_backup[index][i]] = 1;
                        int element = subsets_backup[index][i];
                        newly_covered.push_back(element);
                    }

                    for(int i = 0; i < newly_covered.size(); i++)
                    {
                        for(int j = 0; j < number_subsets; j++)
                        {
                            std::vector<int>::iterator it;
                            it = find(std::begin(subsets_backup[j]), std::end(subsets_backup[j]), newly_covered[i]);
                            if (it != std::end(subsets_backup[j]))
                            subsets_backup[j].erase(it);
                        }
                    }

                }

                int total_cost;

                if(j < 3)
                {
                    //Check time
                    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                    auto time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

                    //Check total cost
                    total_cost = evaluate_solution(selected_subsets, weights);
                    std::cout << "Cost before improvement: " << total_cost << std::endl;
                    
                    //Save results
                    resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100;
                    timefile << "\t" << time_diff;
                }

                /*********************REDUNDANCY ELIMINATION***************************/
                if(j >= 3)
                {            
                    eliminate_redundancy(number_elements, number_subsets, selected_subsets, weights, subsets, selected_subsets_by_element);

                    //Check time
                    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                    auto time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

                    //Check total cost
                    total_cost = evaluate_solution(selected_subsets, weights);
                    std::cout << "Cost before improvement: " << total_cost << std::endl;

                    //Save results
                    resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100;
                    timefile << "\t" << time_diff;
                }

                /******************* IMPROVEMENT ************************/

                std::vector<int> subsets_list (number_subsets);
                for (int i=0; i<1000; ++i) 
                    subsets_list[i] = i;

                std::vector<std::vector<int>> selected_subsets_by_element_backup = selected_subsets_by_element;

                /************** FIRST IMPROVEMENT *******************/
                std::chrono::steady_clock::time_point begin_fi = std::chrono::steady_clock::now();

                std::vector<int> selected_subsets_fi = selected_subsets;
                int to_continue = 1;

                while(to_continue)
                {
                    //Get unselected subsets
                    std::vector<int> unselected_subsets;
                    std::sort(selected_subsets_fi.begin(), selected_subsets_fi.end());
                    std::set_difference(
                        subsets_list.begin(), subsets_list.end(),
                        selected_subsets_fi.begin(), selected_subsets_fi.end(),
                        std::back_inserter( unselected_subsets ));

                    //Shuffle unselected subsets array
                    std::shuffle(unselected_subsets.begin(), unselected_subsets.end(), std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()));

                    //Perform local search
                    to_continue = local_search(true, selected_subsets_fi, unselected_subsets, number_elements, number_subsets, weights, subsets,selected_subsets_by_element);
                }

                //evaluate final solution
                total_cost = evaluate_solution(selected_subsets_fi, weights);

                std::cout << "Cost First: " << total_cost << std::endl;

                //calculate elapsed time
                std::chrono::steady_clock::time_point end_fi = std::chrono::steady_clock::now();
                auto time_diff_fi = std::chrono::duration_cast<std::chrono::microseconds>(end_fi - begin_fi).count();
                
                //save results
                resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100;
                timefile << "\t" << time_diff_fi;

                /**************************BEST IMPROVEMENT****************************/
                std::chrono::steady_clock::time_point begin_bi = std::chrono::steady_clock::now();

                std::vector<int> selected_subsets_bi = selected_subsets;

                to_continue = 1;

                while(to_continue)
                {    
                    std::vector<int> unselected_subsets;
                    //Get unselected subsets
                    std::sort(selected_subsets_bi.begin(), selected_subsets_bi.end());
                    std::set_difference(
                        subsets_list.begin(), subsets_list.end(),
                        selected_subsets_bi.begin(), selected_subsets_bi.end(),
                        std::back_inserter( unselected_subsets ));

                    //std::shuffle(unselected_subsets.begin(), unselected_subsets.end(), std::default_random_engine());

                    //Perform local search
                    to_continue = local_search(false, selected_subsets_bi, unselected_subsets, number_elements, number_subsets, weights, subsets,selected_subsets_by_element_backup);
                }

                //evaluate final solution
                total_cost = evaluate_solution(selected_subsets_bi, weights);
                std::cout << "Cost Best: " << total_cost << std::endl;

                //calculate elapsed time
                std::chrono::steady_clock::time_point end_bi = std::chrono::steady_clock::now();
                auto time_diff_bi = std::chrono::duration_cast<std::chrono::microseconds>(end_bi - begin_bi).count();
                
                //save results
                resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100;
                timefile << "\t" << time_diff_bi;
                
                //end file
                if(j == 5)
                {
                    resultsfile << std::endl;
                    timefile << std::endl;
                }

                //Check feasibility
                std::vector<int> covered (number_elements);

                for(int i = 0; i < selected_subsets.size(); i++)
                {
                    for(int j = 0; j < subsets[selected_subsets[i]].size(); j++)
                    {
                        covered[subsets[selected_subsets[i]][j]] = 1;
                    }
                }

                //std::cout << "Covered size: " << covered.size() << std::endl;
                
                std::cout << "Is it feasible? " << check_elements(covered) << std::endl;

            }

        }

        resultsfile.close();
        timefile.close();
    }

    return 0;
}