#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <bits/stdc++.h>
#include <chrono>
#include <random>

#define DIR "/home/maria/PDEEC/Heuristics/SCP/SCP-Instances/"

// Checks if all the elements are covered
int check_elements(std::vector<int> &covered_elements)
{
    int sum = 0;

    for(int i = 0; i < covered_elements.size(); i++)
    {
        sum += covered_elements[i];
    }

    return sum;
}

// Computes total cost of solution
int evaluate_solution(std::vector<int> &selected_subsets, std::vector<int> &weights)
{
    int total_cost = 0;

    for(int i = 0; i < selected_subsets.size(); i++)
    {
        total_cost += weights[selected_subsets[i]];
    }

    return total_cost;
}

// Dispatch rule of constructive heuristic 1
int ch1(int number_subsets, std::vector<std::vector<int>> &subsets_backup, std::vector<int> &weights){

    int selected_subset;
    std::vector<float> coefficients (number_subsets);

    //compute coefficient for each subset (number of elements of subset / weight of subset)
    for(int i = 0; i < number_subsets; i++)
    {
        coefficients[i] = (float)subsets_backup[i].size()/weights[i];
    }

    //check maximum coefficient
    auto it = std::max_element(std::begin(coefficients), std::end(coefficients));
    selected_subset = std::distance(std::begin(coefficients),it);

    return selected_subset;
}

// Dispatch rule of constructive heuristic 3
int ch3(int number_elements, std::vector<std::vector<int>> &subsets_by_element, std::vector<int> &covered_elements){

    //get rarest element which is not yet covered
    int rare_element = 0;
    int n_subsets = 9999999;
    for(int i = 0; i < number_elements; i++)
    {
        if(subsets_by_element[i].size() < n_subsets && covered_elements[i] == 0)
        {
            rare_element = i;
            n_subsets = subsets_by_element[i].size();
        }
    }

    // select subset which covers said element
    int subset = subsets_by_element[rare_element][0];

    return subset;
}

// Dispatch rule of constructive heuristic 2
int ch2(int number_elements, std::vector<int> &covered_elements, std::vector<std::vector<int>> &subsets_by_element){

    std::vector<int> uncovered;

    for(int i = 0; i < number_elements; i++)
    {
        if(covered_elements[i] == 0)
        {
            uncovered.push_back(i);
        }
    }

    //get random element which is not yet covered
    int random_element = uncovered[rand() % uncovered.size()];

    // select subset which covers said element
    int subset = subsets_by_element[random_element][0];

    return subset;
}

int grasp_constructive(int number_subsets, float alfa, std::vector<std::vector<int>> &subsets_backup, std::vector<int> &weights){

    int selected_subset;
    std::vector<float> coefficients (number_subsets);

    //compute coefficient for each subset (number of elements of subset / weight of subset)
    for(int i = 0; i < number_subsets; i++)
    {
        coefficients[i] = (float)subsets_backup[i].size()/weights[i];
    }

    std::vector<int> indices(coefficients.size());
    std::iota(indices.begin(), indices.end(), 0);
    std::sort(indices.begin(), indices.end(),
           [&](int A, int B) -> bool {
                return coefficients[A] > coefficients[B];
            });

    // keep only the first alfa*size elements
    int new_size = alfa*indices.size();
    indices.resize(new_size);

    //randomly select element
    int random_subset = indices[rand() % indices.size()];

    return random_subset;
}


// Redundancy Elimination Procedure
void eliminate_redundancy(int number_elements, int number_subsets, std::vector<int> &selected_subsets, std::vector<int> &weights, std::vector<std::vector<int>> &subsets, std::vector<std::vector<int>> &selected_subsets_by_element){ 

    while(1)
    {        
        //copy selected subsets
        std::vector<int> redundant_sets = selected_subsets;
        
        //all subsets which are the sole coverers of any element cannot be eliminated
        for(int i = 0; i < number_elements; i++)
        {
            if(selected_subsets_by_element[i].size() == 1)
            {
                auto it = find(std::begin(redundant_sets),std::end(redundant_sets),selected_subsets_by_element[i][0]);
                if(it != std::end(redundant_sets))
                {
                    redundant_sets.erase(it);
                }
            }
        }

        if(redundant_sets.size() == 0)
            return;

        int selected_subset;
        std::vector<float> coefficients (redundant_sets.size());
        float min_coeff = 999999;
        int min_coeff_sub;

        //compute coefficient for each subset (number of elements of subset / weight of subset)
        for(int i = 0; i < redundant_sets.size(); i++)
        {
            coefficients[i] = (float)subsets[redundant_sets[i]].size()/weights[i];

            if(coefficients[i] < min_coeff)
            {
                min_coeff = coefficients[i];
                min_coeff_sub = redundant_sets[i];
            }
        }

        //check minimum coefficient
        auto it = find(std::begin(selected_subsets),std::end(selected_subsets), min_coeff_sub);
        int index = *it;

        // ELIMINATE SUBSET
        selected_subsets.erase(it);

        // Update selected subsets
        for(int i = 0; i < subsets[index].size(); i++)
        {
            int element = subsets[index][i];
            auto to_remove = find(std::begin(selected_subsets_by_element[element]),std::end(selected_subsets_by_element[element]),index);
            if(to_remove != std::end(selected_subsets_by_element[element]))
                selected_subsets_by_element[element].erase(to_remove);
        }
    }
}

// Local Search (First and Best Improvement)
int local_search(bool first, std::vector<int> &selected_subsets, std::vector<int> &unselected_subsets, int number_elements, int number_subsets, std::vector<int> &weights, std::vector<std::vector<int>> &subsets, std::vector<std::vector<int>> &selected_subsets_by_element){

    int init_total_cost = evaluate_solution(selected_subsets, weights), new_total = init_total_cost, new_best = init_total_cost;

    for(int i = 0; i < unselected_subsets.size(); i++)
    {        
        //add subset in line to solution
        std::vector<int> selected_subsets_fn = selected_subsets;
        selected_subsets_fn.push_back(unselected_subsets[i]);

        std::vector<std::vector<int>> selected_subsets_by_element_backup = selected_subsets_by_element;

        for(int j = 0; j < subsets[unselected_subsets[i]].size(); j++)
        {
            selected_subsets_by_element_backup[subsets[unselected_subsets[i]][j]].push_back(unselected_subsets[i]);
        }

        //eliminate redundancy
        eliminate_redundancy(number_elements, number_subsets, selected_subsets_fn, weights, subsets, selected_subsets_by_element_backup);    
        
        //check if new solution is better than initial
        new_total = evaluate_solution(selected_subsets_fn,weights);
        if(new_total < new_best)
        {
            selected_subsets = selected_subsets_fn;
            selected_subsets_by_element = selected_subsets_by_element_backup;
            new_best = new_total; 
            
            // exit if strategy is First Improvement
            if(first)
                return new_total;
        }
    }

    if(new_best >= init_total_cost)
    {
        return 0;
    }
}

int main(){
    
    std::cout << "Hello" << std::endl;

    //Set parameters
    float alfa = 0.1;

    for(int k = 0; k < 5; k++)
    {

        std::cout << "Running " << k << std::endl;

        //open intances file
        std::ifstream genfile(DIR + std::string("instances.txt"));
        std::string instance;
        int benchmark;

        //open results file
        std::ofstream resultsfile, timefile;
        resultsfile.open(DIR + std::string("grasp/complete/alfa01/results" + std::to_string(k+1) + ".csv"));
        timefile.open(DIR + std::string("grasp/complete/alfa01/time" + std::to_string(k+1) + ".csv"));
        resultsfile << "Inst \t Benchmark \t It 20 Avg \t It 50 Avg \t It 100 Avg \t It 200 Avg \n";
        timefile << "Inst \t It 20 \t It 50 \t It 100 \t It 200 \n";

        // for each instance
        while(genfile >> instance)
        {
            //open instance file
            std::ifstream instfile(DIR + std::string("scp") + instance + std::string(".txt"));
            //std::ifstream instfile(DIR + std::string("scp42.txt"));

            std::cout << "Instance " << instance << std::endl;

            genfile >> benchmark;
            resultsfile << instance << "\t" << benchmark;
            timefile << instance;

            //save number of subsets and number of elements
            int number_elements, number_subsets;
            instfile >> number_elements >> number_subsets;

            //save weight of each subset
            std::vector<int> weights;
            for(int i = 0; i < number_subsets; i++)
            {
                int weight;
                instfile >> weight;
                weights.push_back(weight);
            }

            std::vector<std::vector<int>> subsets;
            subsets.resize(number_subsets);

            std::vector<std::vector<int>> subsets_by_element;
            subsets_by_element.resize(number_elements);

            //save elements of each subset
            for(int i = 0; i < number_elements; i++)
            {
                int size;
                instfile >> size;
                for(int j = 0; j < size; j++)
                {
                    int subset;
                    instfile >> subset;
                    subsets[subset-1].push_back(i);
                    subsets_by_element[i].push_back(subset-1);
                }
            }

            //start counter
            std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        
            int n_iterations = 200;
            std::vector<int> current_best_solution;
            int current_total_cost = 999999999;
            int max_total_cost = 0;
            int total_cost_sum = 0;

            for(int j = 0; j < n_iterations; j++)
            {
            
                //create auxiliary structures
                std::vector<std::vector<int>> subsets_backup = subsets;

                //create output structures
                std::vector<int> covered_elements (number_elements);
                std::vector<int> selected_subsets;
                std::vector<std::vector<int>> selected_subsets_by_element;
                selected_subsets_by_element.resize(number_elements);

                
                // CONSTRUCTIVE HEURISTICS
                while(check_elements(covered_elements) < number_elements)
                {
                    int selected_subset = grasp_constructive(number_subsets, alfa, subsets_backup, weights);

                    //add subset to selected subsets
                    selected_subsets.push_back(selected_subset);

                    //add subset to selected subsets by element
                    for(int i = 0; i < subsets[selected_subset].size(); i++)
                    {
                        selected_subsets_by_element[subsets[selected_subset][i]].push_back(selected_subset);
                    }

                    int size = subsets_backup[selected_subset].size();
                    std::vector<int> newly_covered;

                    //update covered elements
                    for(int i = 0; i < size; i++)
                    {
                        //update covered elements
                        covered_elements[subsets_backup[selected_subset][i]] = 1;
                        int element = subsets_backup[selected_subset][i];
                        newly_covered.push_back(element);
                    }

                    for(int i = 0; i < newly_covered.size(); i++)
                    {
                        for(int k = 0; k < number_subsets; k++)
                        {
                            std::vector<int>::iterator it;
                            it = find(std::begin(subsets_backup[k]), std::end(subsets_backup[k]), newly_covered[i]);
                            if (it != std::end(subsets_backup[k]))
                            subsets_backup[k].erase(it);
                        }
                    }

                }

                //eliminate_redundancy(number_elements, number_subsets, selected_subsets, weights, subsets, selected_subsets_by_element);

                //Check total cost
                int total_cost = evaluate_solution(selected_subsets, weights);       

                //LOCAL SEARCH

                std::vector<int> subsets_list (number_subsets);
                for (int i=0; i<1000; ++i) 
                    subsets_list[i] = i;

                std::vector<std::vector<int>> selected_subsets_by_element_backup = selected_subsets_by_element;

                std::vector<int> selected_subsets_bi = selected_subsets;

                int to_continue = 1;

                while(to_continue)
                {    
                    std::vector<int> unselected_subsets;
                    //Get unselected subsets
                    std::sort(selected_subsets_bi.begin(), selected_subsets_bi.end());
                    std::set_difference(
                        subsets_list.begin(), subsets_list.end(),
                        selected_subsets_bi.begin(), selected_subsets_bi.end(),
                        std::back_inserter( unselected_subsets ));

                    //Perform local search
                    to_continue = local_search(false, selected_subsets_bi, unselected_subsets, number_elements, number_subsets, weights, subsets,selected_subsets_by_element_backup);
                }

                //evaluate final solution
                total_cost = evaluate_solution(selected_subsets_bi, weights);

                //update minimum cost
                if(total_cost < current_total_cost)
                {
                    current_best_solution = selected_subsets_bi;
                    current_total_cost = total_cost;
                }
                //update maximum cost
                if(total_cost > max_total_cost)
                {
                    max_total_cost = total_cost;
                }
                //update sum for further average computation
                total_cost_sum += total_cost;

                if(j == 19 || j == 49 || j == 99 || j == 199)
                {
                    //calculate elapsed time
                    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
                    auto time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
                    
                    //save results
                    resultsfile << "\t" << current_total_cost;
                    timefile << "\t" << time_diff;

                    if(j == 199)
                    {
                        resultsfile << std::endl;
                        timefile << std::endl;
                    }
                }

            }

            std::cout << "This instance's best: " << current_total_cost << std::endl;

        }

        resultsfile.close();
        timefile.close();

    }

    return 0;
}