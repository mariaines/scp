#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <bits/stdc++.h>
#include <chrono>

#define DIR "/home/maria/PDEEC/Heuristics/SCP/SCP-Instances/"

int check_elements(std::vector<int> &covered_elements)
{
    int sum = 0;

    for(int i = 0; i < covered_elements.size(); i++)
    {
        sum += covered_elements[i];
    }

    return sum;
}

int evaluate_solution(std::vector<int> &selected_subsets, std::vector<int> &weights)
{
    int total_cost = 0;

    for(int i = 0; i < selected_subsets.size(); i++)
    {
        total_cost += weights[selected_subsets[i]];
    }

    return total_cost;
}

int ch1(int number_subsets, std::vector<std::vector<int>> &subsets_backup, std::vector<int> &weights){

    int selected_subset;
    std::vector<float> coefficients (number_subsets);

    //compute coefficient for each subset (number of elements of subset / weight of subset)
    for(int i = 0; i < number_subsets; i++)
    {
        coefficients[i] = (float)subsets_backup[i].size()/weights[i];
        //std::cout << "size " << i << ": " << subsets_backup[i].size() << std::endl;
    }

    //check maximum coefficient
    auto it = std::max_element(std::begin(coefficients), std::end(coefficients));
    selected_subset = std::distance(std::begin(coefficients),it);
    //std::cout << "selected_subset: " << selected_subset << std::endl;

    return selected_subset;
}

int ch3(int number_elements, std::vector<int> &covered_elements, std::vector<std::vector<int>> &subsets_by_element){

    //get rarest element which is not yet covered
    int rare_element = 0;
    int n_subsets = 9999999;
    for(int i = 0; i < number_elements; i++)
    {
        if(subsets_by_element[i].size() < n_subsets && covered_elements[i] == 0)
        {
            rare_element = i;
            n_subsets = subsets_by_element[i].size();
        }
    }

    // select subset which covers said element
    int subset = subsets_by_element[rare_element][0];

    return subset;
}

int ch2(int number_elements, std::vector<int> &covered_elements, std::vector<std::vector<int>> &subsets_by_element){

    std::vector<int> uncovered;

    for(int i = 0; i < number_elements; i++)
    {
        if(covered_elements[i] == 0)
        {
            uncovered.push_back(i);
            //std::cout << "Heyo" << i << std::endl;
        }
        //std::cout << "state: " << covered_elements[i] << std::endl;
    }

    //get random element which is not yet covered
    int random_element = uncovered[rand() % uncovered.size()];

    // select subset which covers said element
    int subset = subsets_by_element[random_element][0];

    //std::cout << "subset: " << subset << std::endl;

    return subset;
}

void eliminate_redundancy(int number_elements, std::vector<int> &selected_subsets, std::vector<int> &weights, std::vector<std::vector<int>> &subsets, std::vector<std::vector<int>> &selected_subsets_by_element){ 

    while(1)
    {
        //copy selected subsets
        std::vector<int> redundant_sets = selected_subsets;
        
        for(int i = 0; i < number_elements; i++)
        {
            if(selected_subsets_by_element[i].size() == 1)
            {
                auto it = find(std::begin(redundant_sets),std::end(redundant_sets),selected_subsets_by_element[i][0]);
                if(it != std::end(redundant_sets))
                {
                    redundant_sets.erase(it);
                }
            }
        }

        //std::cout << "Size: " << redundant_sets.size() << std::endl;

        if(redundant_sets.size() == 0)
        break;

        int max_weight = 0;
        int max_weight_subset;

        // find subset with the largest weight among those which can be eliminated
        for(int i = 0; i < redundant_sets.size(); i++)
        {
            if(weights[redundant_sets[i]] > max_weight)
            {
                max_weight = weights[redundant_sets[i]];
                max_weight_subset = redundant_sets[i];
            }
        }

        auto it = find(std::begin(selected_subsets),std::end(selected_subsets),max_weight_subset);
        int index = *it;

        // ELIMINATE SUBSET
        selected_subsets.erase(it);
        //std::cout << "Removed subset: " << max_weight_subset << std::endl;

        for(int i = 0; i < subsets[index].size(); i++)
        {
            int element = subsets[index][i];
            auto to_remove = find(std::begin(selected_subsets_by_element[element]),std::end(selected_subsets_by_element[element]),index);
            if(to_remove != std::end(selected_subsets_by_element[element]))
            selected_subsets_by_element[element].erase(to_remove);
        }

    }
}

int main(){
    
    std::cout << "Hello" << std::endl;

    //open intances file
    std::ifstream genfile(DIR + std::string("instances.txt"));
    std::string instance;
    int benchmark;

    //open results file
    std::ofstream resultsfile;
    resultsfile.open(DIR + std::string("constructive/results5.csv"));
    resultsfile << "Instance \t Benchmark \t CH1 \t Deviation \t Time \t CH1RE \t Deviation \t Time \t CH2 \t Deviation \t Time \t CH2RE \t Deviation \t Time \t CH3 \t Deviation \t Time \t CH3RE \t Deviation \t Time \t \n";

    int n_heuristics = 3;

    srand (time(NULL));

    while(genfile >> instance)
    {
        //open instance file
        std::ifstream instfile(DIR + std::string("scp") + instance + std::string(".txt"));

        std::cout << "Instance " << instance << std::endl;

        genfile >> benchmark;
        resultsfile << instance << "\t" << benchmark;

        //save number of subsets and number of elements
        int number_elements, number_subsets;
        instfile >> number_elements >> number_subsets;
        //std::cout << number_elements << " elements and " << number_subsets << " subsets." << std::endl;

        //save weight of each subset
        std::vector<int> weights;
        for(int i = 0; i < number_subsets; i++)
        {
            int weight;
            instfile >> weight;
            weights.push_back(weight);
            //std::cout << weights[i] << std::endl;
        }

        std::vector<std::vector<int>> subsets;
        subsets.resize(number_subsets);

        std::vector<std::vector<int>> subsets_by_element;
        subsets_by_element.resize(number_elements);

        //save elements of each subset
        for(int i = 0; i < number_elements; i++)
        {
            int size;
            instfile >> size;
            for(int j = 0; j < size; j++)
            {
                int subset;
                instfile >> subset;
                //std::cout << subset << std::endl;
                subsets[subset-1].push_back(i);
                subsets_by_element[i].push_back(subset-1);
                //std::cout << subsets[subset-1][0] << std::endl;
            }
        }

        for(int j = 0; j < n_heuristics; j++)
        {
            //start counter
            std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        
            //create auxiliary structures
            std::vector<std::vector<int>> subsets_backup = subsets;

            //create output structures
            std::vector<int> covered_elements (number_elements);
            std::vector<int> selected_subsets;
            std::vector<std::vector<int>> selected_subsets_by_element;
            selected_subsets_by_element.resize(number_elements);

            while(check_elements(covered_elements) < number_elements)
            {
                //Select subset to add to the solution (Dispatch Rule)
                int index;
                if(j == 0)  index = ch1(number_subsets, subsets_backup, weights);
                else if(j == 1) index = ch2(number_elements, covered_elements, subsets_by_element);
                else if(j == 2) index = ch3(number_elements, covered_elements, subsets_by_element);

                //add subset to selected subsets
                selected_subsets.push_back(index);

                //add subset to selected subsets by element
                for(int i = 0; i < subsets[index].size(); i++)
                {
                    selected_subsets_by_element[subsets[index][i]].push_back(index);
                }

                int size = subsets_backup[index].size();
                std::vector<int> newly_covered;

                //update covered elements
                for(int i = 0; i < size; i++)
                {
                    //update covered elements
                    covered_elements[subsets_backup[index][i]] = 1;
                    int element = subsets_backup[index][i];
                    newly_covered.push_back(element);
                }

                for(int i = 0; i < newly_covered.size(); i++)
                {
                    for(int j = 0; j < number_subsets; j++)
                    {
                        std::vector<int>::iterator it;
                        it = find(std::begin(subsets_backup[j]), std::end(subsets_backup[j]), newly_covered[i]);
                        if (it != std::end(subsets_backup[j]))
                        subsets_backup[j].erase(it);
                    }
                }

                //break;

            }

            // Check results
            //std::cout << selected_subsets.size() << std::endl;
            //std::cout << evaluate_solution(selected_subsets, weights) << std::endl;

            //Check time
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            //std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << std::endl;
            auto time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

            int total_cost = evaluate_solution(selected_subsets, weights);

            resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100 << "\t" << time_diff;

            /*********************REDUNDANCY ELIMINATION***************************/
            eliminate_redundancy(number_elements, selected_subsets, weights, subsets, selected_subsets_by_element);

            //Check results
            //std::cout << evaluate_solution(selected_subsets, weights) << std::endl; 

            //Check time
            end = std::chrono::steady_clock::now();
            time_diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

            total_cost = evaluate_solution(selected_subsets, weights);

            resultsfile << "\t" << total_cost << "\t" << (float)(total_cost-benchmark)/benchmark*100 << "\t" << time_diff;
            
            if(j == 2)
                resultsfile << "\n";

        }

    }

    resultsfile.close();

    return 0;
}